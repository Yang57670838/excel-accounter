const electron = require('electron')
const fs = require('fs')
const { ipcRenderer, shell, remote, desktopCapturer, nativeImage } = electron
const { saveFileName, getFileName, removeFileName } = require('./utils')
const { yearSelection } = require('./constants')

const savedFile = getFileName()
if (savedFile) {
    document.getElementById("formattedFileName").innerHTML = savedFile
}

document.querySelector('form').addEventListener('submit', ()=>{
    event.preventDefault();
    const { path } = document.querySelector('input').files[0];
    // ipcRenderer.send('excel:submit', path);

    const selectedYear = document.getElementById("yearSelect").selectedIndex
    let response = ipcRenderer.sendSync('excel:submit', {
        path,
        selectedYear: yearSelection[selectedYear]
    })
    if (response) {
        new Notification('excel app', {
            body: `成功转化excel文件, 命名为${response}`
        })
        saveFileName(response)
        document.getElementById("formattedFileName").innerHTML = response
        const desktopPath = remote.app.getPath('desktop')
        shell.showItemInFolder(`${desktopPath}/${response}`)
    }
})

// ipcRenderer.on('excel:submitResponse', (e, args) => {
//     console.log(args)
// })

document.getElementById('openFileButton').addEventListener("click", openFormattedExcel)
function openFormattedExcel() {
    // check localstorage has formatted file or not, if not, show error message
    const savedFile = getFileName()
    if (savedFile) { 
        const desktopPath = remote.app.getPath('desktop')
        const formattedFile = `${desktopPath}/${savedFile}`
        shell.showItemInFolder(formattedFile)
    } else {
        // not found file
        remote.dialog.showMessageBox({type: 'warning', buttons: ['关闭'], title: 'Excel app', message: '先转化文件'}, (buttonIndex) => {
        })
    } 
}

document.getElementById('screenshotButton').addEventListener("click", screenshot)
function screenshot() {
    desktopCapturer.getSources({ types: ['window'], thumbnailSize: {width: 1920, height: 1080} }, (err, sources) => {
        const jpgImage = sources[0].thumbnail.toJPEG(100)
        saveFileToDesktop(jpgImage, 'jpg')   
    }) 
}

const saveFileToDesktop = (data, type) => {
    const desktopPath = remote.app.getPath('desktop')
    const currentDateTime = getCurrentDateAndTime()
    fs.writeFile(`${desktopPath}/screenshot${currentDateTime}.${type}`, data, err => {
        if (err) throw err
        shell.showItemInFolder(`${desktopPath}/screenshot${currentDateTime}.${type}`)
    })
}

const getCurrentDateAndTime = () => {
    const today = new Date()
    const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
    const time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds()
    const dateTime = date+'-'+time
    return dateTime
}

document.getElementById('removeFileButton').addEventListener("click", removeFormattedExcelRecord)
function removeFormattedExcelRecord() {
    removeFileName()
    document.getElementById("formattedFileName").innerHTML = ''
}
