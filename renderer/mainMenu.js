const { app } = require('electron')

module.exports = [
    { 
        label: 'Excel',
        submenu: [
            { 
                label: '全屏',
                role: 'toggleFullScreen'
            },
            { 
                label: '退出',
                click: () => { app.quit()},
                accelerator: 'CommandOrControl+4'
            },
        ]
     }
];