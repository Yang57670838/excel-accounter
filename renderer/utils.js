exports.saveFileName = (fileName) => {
    localStorage.setItem('formattedExcelFile', fileName)
}

exports.getFileName = () => {
    return localStorage.getItem('formattedExcelFile')
}

exports.removeFileName = () => {
    localStorage.removeItem('formattedExcelFile')
}