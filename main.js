const electron = require('electron')
const { app, BrowserWindow, dialog, globalShortcut, Menu, ipcMain  } = electron
const windowStateKeeper = require('electron-window-state')
var moment = require('moment')

// not for prod env
// require('electron-reload')(__dirname)

var xlsx = require('xlsx')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
// let childWindow
let mainMenu = Menu.buildFromTemplate( require('./renderer/mainMenu') )

let contextMenu = Menu.buildFromTemplate([
    {
        label: '退出',
        click: () => { app.quit()}
    }
])

const createWindow = () => {

    // require('devtron').install()
    let mainWinState = windowStateKeeper({
        defaultWidth: 800,
        defaultHeight: 600
    })

    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: mainWinState.width,
        height: mainWinState.height,
        x: mainWinState.x,
        y: mainWinState.y,
        webPreferences: {
            nodeIntegration: true,
        },
        show: false,
        backgroundColor: '#696969',
        icon: `${__dirname}/assets/icons/png/64x64.png`
    })

    let mainSession = mainWindow.webContents.session
    // childWindow = new BrowserWindow({
    //     width: 400,
    //     height: 300,
    //     webPreferences: {
    //         nodeIntegration: true
    //     },
    //     show: false,
    //     parent: mainWindow,
    //     modal: true
    // })

    mainWinState.manage(mainWindow)

    // and load the index.html of the app.
    // mainWindow.loadFile('index.html')
    mainWindow.loadURL(`file://${__dirname}/renderer/main.html`)
    // childWindow.loadURL(`file://${__dirname}/index_child.html`)

    // mainSession.cookies.set({url: 'https://excelApp.com', value: '2222', name: 'excelResults', expirationDate: 999999999999}, err => {
    //     mainSession.cookies.get({name: 'excelResults'}, (err, cookies) => {
    //         console.log(cookies)
    //     })
    // })

    Menu.setApplicationMenu(mainMenu)
    mainWindow.webContents.on('context-menu', e => {
        contextMenu.popup()
    })
    
    let mainContents = mainWindow.webContents

    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    })
    // childWindow.once('ready-to-show', () => {
    //     childWindow.show()
    // })

    // Open the DevTools. not for prod env
    // mainContents.openDevTools()

    mainContents.on('will-navigate', (e, url) => {
        console.log('will navigate to :' +url)
    })

    mainContents.on('did-navigate', (e, url) => {
        console.log('did navigate to :' +url)
    })

    // help to login to some pages which has basic auth login pop up(user name, password)
    mainContents.on('login', (e, req, authInfo, callback) => {
        e.preventDefault()
        callback('tempUsername', 'tempPassword')
    })

    //monitor text user selected by right click
    mainContents.on('context-menu', (e, params) => {
        console.log('User selected text :' + params.selectionText)
        console.log('selection can be copied :' + params.editFlags.canCopy)
    })

    // childWindow.on('blur', () => {
    //     childWindow.close()
    // })

    globalShortcut.register('CommandOrControl+1', () => {
        console.log('User press CommandOrControl+1')
    })
    globalShortcut.register('CommandOrControl+4', () => {
        app.quit()
    })

    // mainWindow.webContents.on('did-finish-load', e=> {
    //     mainWindow.webContents.send('mailbox',{
    //         from: 'Jerry',
    //         email: 'j@test.com'
    //     })
    // })

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time



      // when you should delete the corresponding element.
      mainWindow = null
    })
    // childWindow.on('closed', () => {
    //     childWindow = null
    // })

    electron.powerMonitor.on('resume', e => {
        if (!mainWindow) {
            createWindow()
        }
        showMessageDialog('最近更改了n个文件') // change to dynamic persist session data n later..
    })

    ipcMain.on('cookie:get', (e, args) => {
        mainSession.cookies.get({name: 'excelResults'}, (err, cookies) => {
            e.returnValue = cookies[0]
        })
    })
    console.log('hello from main process')
}

app.on('before-quit', e => {
    console.log('app is about to quit')
})

app.on('browser-window-blur', e => {
    // console.log('window out of focus')
})

app.on('browser-window-focus', e => {
    // console.log('window in focus')
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {

    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
      createWindow()
    }
})

  // In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
function showMessageDialog(message) {
    dialog.showMessageBox({buttons: ['关闭'], title: 'Excel app', message: message}, (buttonIndex) => {
        console.log('selected', buttonIndex)
    })
}

ipcMain.on('excel:submit', (e, args) => {
    const now = moment().format('YYYY-MM-DD-h-mm-ss')
    const desktopPath = app.getPath('desktop')
    const workbook = xlsx.readFile(args.path, {cellDates: true})
    let objectValue = []
    const sheetList = workbook.SheetNames
    const displayValue = []

    // second workbook for show only filtered result in new file with categories
    const secondWB = xlsx.utils.book_new()

    for (let i=1; i<sheetList.length; i++) {
        const colValue = xlsx.utils.sheet_to_json(workbook.Sheets[sheetList[i]])
        const filteredColValue = colValue.filter(e => {
            var ob = Object.values(e)
            // compare year number from moment to year string from renderer selection, use == istead of ===
            if (typeof ob[ob.length-1] === 'string' && ob[ob.length-1].localeCompare('投资款') === 0 && moment(ob[0]).year() == args.selectedYear) {
                return true
            } else {
                return false
            }
        })
        
        // for display in window
        if (filteredColValue.length !== 0) {
            const display = {
                key:  sheetList[i],
                value: filteredColValue
            }
            displayValue.push(display)

            const secondWS = xlsx.utils.json_to_sheet(filteredColValue)
            xlsx.utils.book_append_sheet(secondWB, secondWS, sheetList[i])
        }
        
        // for generate excel file
        objectValue = objectValue.concat(filteredColValue)
    }
    console.log('final result', objectValue)
    console.log('displayValue', displayValue)

    // create new work book file into desktop
    const newWB = xlsx.utils.book_new()
    const newWS = xlsx.utils.json_to_sheet(objectValue)
    xlsx.utils.book_append_sheet(newWB, newWS, sheetList[0])
    for (let i=1; i<sheetList.length; i++) {
        const tempWS = workbook.Sheets[sheetList[i]]
        xlsx.utils.book_append_sheet(newWB, tempWS, sheetList[i])
    }
    xlsx.writeFile(newWB, `${desktopPath}/${now}.xlsx`)

    // second excel file generate
    xlsx.writeFile(secondWB, `${desktopPath}/result-${now}.xlsx`)

    // response
    // e.sender.send('excel:submitResponse', true)
    e.returnValue = `${now}.xlsx`
})
